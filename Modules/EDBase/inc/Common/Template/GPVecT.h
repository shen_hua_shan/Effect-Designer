//-------------------------------------------------------
// Copyright (c) PPCore
// All rights reserved.
// 
// File Name: GPVecT.h 
// File Des: Vec封装类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      guoyou		2015-7-21	1.0			
//-------------------------------------------------------
#pragma once

/// <summary>
///		统一的Vec模板
/// </summary>
template<class TObj>
class GPVecT
{
public:
	GPVecT(){}
	~GPVecT(){/*RemoveAll必须由重载函数的子类调用，不然重载函数无效*/}


	/// -------------------------------------------------
	/// @brief 获取数目
	/// @return 数目
	int GetCount()
	{
		return (int)m_GPVec.size();
	}

	/// -------------------------------------------------
	/// @brief obj是否存在
	/// @param[in]		 obj    用于查找的obj
	/// @return PP_INVALID_VALUE(-1)表示不存在
	int FindObj(const TObj &obj)
	{
		int count = (int)m_GPVec.size();
		for (int i=0; i<count; i++)
		{
			if (EqualVecObj(obj, m_GPVec[i]))
			{
				return i;
			}
		}

		return PP_INVALID_VALUE;
	}

	/// -------------------------------------------------
	/// @brief 取得obj,
	/// @param[in]  iElement		指定项的index
	/// @param[out] obj			    返回指定项的obj
	/// @remark 安全获取方式，推荐
	/// @return false:获取失败,true:获取成功
	bool GetObj(int iElement, TObj &obj)
	{
		bool bRet = false;
		do 
		{
			if (iElement>=GetCount())
			{
				break;
			}

			obj = m_GPVec[iElement];
			bRet = true;
		} while (false);
		return true;
	}

	/// -------------------------------------------------
	/// @brief 取得obj
	/// @param[in] iElement		   指定项的index
	/// @remark 不安全获取方式
	/// @return 获取的对象
	TObj &GetObj(int iElement)
	{
		return m_GPVec[iElement];
	}

	/// -------------------------------------------------
	/// @brief 加入obj
	/// @param[in] obj			   加入指定项的obj
	/// @return true:加入成功,false:obj已存在
	bool AddObj(const TObj &obj)
	{
		bool bRet = false;
		do 
		{
			if (PP_INVALID_VALUE != FindObj(obj))/// obj已存在
			{
				break;
			}
			m_GPVec.push_back(obj);
			bRet = true;
		} while (false);
		return bRet;
	}

	/// -------------------------------------------------
	/// @brief 移除obj
	/// @param[in] iElement		指定项的index
	/// @return true:移除成功,false:移除成功
	bool RemoveObj(int iElement)
	{
		bool bRet = false;
		do 
		{
			if (iElement>=GetCount())
			{
				break;
			}
			PreVecObjRemove(m_GPVec[iElement]);
			m_GPVec.erase(m_GPVec.begin()+iElement);
			bRet = true;
		} while (false);
		return bRet;
	}

	/// -------------------------------------------------
	/// @brief 移除obj
	/// @param[in] obj		指定项obj
	/// @return true:移除成功,false:移除成功
	bool RemoveObj(TObj &obj)
	{
		bool bRet = false;
		do 
		{
			int iFind = FindObj(obj);
			if (-1 == iFind)//obj不存在
			{
				break;
			}
			PreVecObjRemove(obj);
			m_GPVec.erase(m_GPVec.begin()+iFind);
			bRet = true;
		} while (false);
		return bRet;
	}

	/// -------------------------------------------------
	/// @brief 移除所有obj
	/// @return 无
	void RemoveAll()
	{
		int count = GetCount();
		for (int i=0; i<count; i++)
		{
			PreVecObjRemove(m_GPVec[i]);
		}

		m_GPVec.clear();
	}

	/// -------------------------------------------------
	/// @brief 可重载
	/// @remark 比较函数，由子类重载，默认直接==
	/// @return true:相等,false:不相等
	virtual bool EqualVecObj(const TObj &objsrc, const TObj &objdest)
	{
		/// 外部子类重载
		if (objsrc == objdest)
		{
			return true;
		}
		return false;
	}

	/// -------------------------------------------------
	/// @brief 可重载
	/// @param[in] obj		指定项
	/// @remark 在移除前的预处理函数，用于子类重载
	/// @return 无
	virtual void PreVecObjRemove(const TObj &obj)
	{
		/// 外部子类重载
	}
public:
	std::vector<TObj>         m_GPVec;
};

