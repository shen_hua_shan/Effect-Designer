// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDResEditDlg.h 
// File mark:   
// File summary:资源添加和编辑的弹出对话框
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-6-10
// ----------------------------------------------------------------
#pragma once

int Show_ResEditDlg(LPCWSTR lpResTag, bool bMultiFiles, LPCWSTR lpResName, HWND hWnd, CStringW& strOutTag, CStringW& strOutName, bool bDisableCombobox = false);
class EDResEditDlg : public DMHDialog
{
public:
	EDResEditDlg();

private:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_MAP()
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	void OnSize(UINT nType, CSize size);
	DMCode OnBtnClick(int uID);
	DMCode OnBtnClose();
	DMCode OnBtnOk();
public:
	DMSmartPtrT<DUIRichEdit>		m_pNameEdit;
	CStringW                        m_strNameText;
	CStringW                        m_strTag;
	bool							m_bDisableCombobox;
	bool							m_bMultiFiles;
};