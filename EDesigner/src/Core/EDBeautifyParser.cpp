#include "StdAfx.h"
#include "EDBeautifyParser.h"

EDBeautifyPartsParser::EDBeautifyPartsParser()
{

}

EDBeautifyPartsParser::~EDBeautifyPartsParser()
{

}

DM::DMCode EDBeautifyPartsParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];//"beautify"
	return EDJSonParser::BuildJSonData(JSonChild);
}


EDBeautifyParser::EDBeautifyParser()
	: m_dbEnlargeRatio(0.13)
	, m_dbShrinkRatio(0.1)
	, m_dbSmallRatio(0.1)
	, m_dbReddenStrength(0.36)
	, m_dbWhitenStrength(0.3)
	, m_dbSmoothStrength(0.74)
	, m_dbContrastStrength(0.05)
	, m_dbSaturation(0.5)
{
}

EDBeautifyParser::~EDBeautifyParser()
{
}

DM::DMCode EDBeautifyParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_contrastStrength))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbContrastStrength = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_enlargeRatio))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbEnlargeRatio = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_reddenStrength))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbReddenStrength = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_saturation))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbSaturation = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_shrinkRatio))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbShrinkRatio = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_smallRatio))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbSmallRatio = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_smoothStrength))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbSmoothStrength = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDBeautifyParserAtrr::DOUBLE_whitenStrength))
	{
		if (JsHandleValue.isDouble())
		{
			m_dbWhitenStrength = JsHandleValue.toDouble();
			iErr = DM_ECODE_OK;
		}
	}
	else
	{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
		DMASSERT_EXPR(false, L"Beautify 有没有解析的节点");
		iErr = DM_ECODE_NOLOOP;
	}
	if (!bLoadJSon)
		MarkDataDirty();
	return iErr;
}

DM::DMCode EDBeautifyParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DM::DMCode EDBeautifyParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_enlargeRatio].putDouble(m_dbEnlargeRatio);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_shrinkRatio].putDouble(m_dbShrinkRatio);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_smallRatio].putDouble(m_dbSmallRatio);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_reddenStrength].putDouble(m_dbReddenStrength);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_whitenStrength].putDouble(m_dbWhitenStrength);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_smoothStrength].putDouble(m_dbSmoothStrength);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_contrastStrength].putDouble(m_dbContrastStrength);
	JSonHandler[EDAttr::EDBeautifyParserAtrr::DOUBLE_saturation].putDouble(m_dbSaturation);

	return DM_ECODE_FAIL;
}