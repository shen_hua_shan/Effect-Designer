#include "stdafx.h"
#include "DMTooltipReImpl.h"

namespace DM
{
#define TIMERID_DELAY    1
#define TIMERID_SPAN     2
#define MARGIN_TIP       5

	BEGIN_MSG_MAP(DMTooltipReImpl)
		MSG_WM_PAINT(OnPaint)
		MSG_WM_TIMER(OnTimer)
		MSG_WM_SIZE(OnSize)
		REFLECT_NOTIFICATIONS_EX()
	END_MSG_MAP()

	DMTooltipReImpl::DMTooltipReImpl(void)
	:m_iDelayTime(500),
	m_iSpanTime(5000),
	m_font(NULL),
	m_bShowing(false)
	{
	}

	DMTooltipReImpl::~DMTooltipReImpl(void)
	{
		if (m_font) 
		{
			DeleteObject(m_font);
		}
	}

	DMTooltipReImpl*	  DMTooltipReImpl::ms_pCurTipWnd = NULL;
	bool DMTooltipReImpl::Create()
	{
		bool bRet = false;
		do 
		{
			ATOM Atom = g_pDMApp->GetClassAtom(false/*true*/);  // 是否使用阴影窗口类创建
			HWND hWnd = DMCWnd::CreateWindowEx((LPCWSTR)Atom,L"tooltip",WS_POPUP,WS_EX_TOOLWINDOW|WS_EX_TOPMOST|WS_EX_NOACTIVATE,0,0,0,0,NULL,NULL);
			if (NULL == hWnd)
			{
				break;
			}

			LOGFONTW lf;
			GetObject(GetStockObject(DEFAULT_GUI_FONT),sizeof(lf),&lf);
			lf.lfHeight = -12;
			wcscpy_s(lf.lfFaceName, 32, L"微软雅黑");
			m_font = CreateFontIndirectW(&lf);
			bRet = true;
			break;

			CSize sz = { 200, 150 };
			ms_pCurTipWnd = new DMTooltipReImpl;
			hWnd = ((DMCWnd*)ms_pCurTipWnd)->CreateWindowEx((LPCWSTR)Atom, NULL, WS_POPUP, WS_EX_TRANSPARENT | WS_EX_TOOLWINDOW | WS_EX_TOPMOST, 0, 0, sz.cx, sz.cy, NULL, NULL);
			if (NULL == hWnd)
			{
				DM_DELETE(ms_pCurTipWnd);
				break;
			}
			ms_pCurTipWnd->ModifyStyleEx(0, WS_EX_LAYERED);
			HDC hdc = ms_pCurTipWnd->GetDC();

			HDC hMemDC = ::CreateCompatibleDC(hdc);
			//DMDragWnd::DragBegin(m_pDragCanvas, pt, 0, 128, LWA_ALPHA | LWA_COLORKEY);

			BITMAPINFO bitmapinfo;
			bitmapinfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			bitmapinfo.bmiHeader.biBitCount = 32;
			bitmapinfo.bmiHeader.biHeight = sz.cx;
			bitmapinfo.bmiHeader.biWidth = sz.cy;
			bitmapinfo.bmiHeader.biPlanes = 1;
			bitmapinfo.bmiHeader.biCompression = BI_RGB;
			bitmapinfo.bmiHeader.biXPelsPerMeter = 0;
			bitmapinfo.bmiHeader.biYPelsPerMeter = 0;
			bitmapinfo.bmiHeader.biClrUsed = 0;
			bitmapinfo.bmiHeader.biClrImportant = 0;
			bitmapinfo.bmiHeader.biSizeImage = bitmapinfo.bmiHeader.biWidth * bitmapinfo.bmiHeader.biHeight * bitmapinfo.bmiHeader.biBitCount / 8;
			HBITMAP hBitmap = ::CreateDIBSection(hMemDC, &bitmapinfo, 0, NULL, 0, 0);
			HBITMAP hOldBitmap = (HBITMAP)::SelectObject(hMemDC, hBitmap);

			RECT rc;
			SetRect(&rc, 0, 0, sz.cx, sz.cy);
			DrawText(hMemDC, L"Center Line Text", - 1, &rc, DT_VCENTER | DT_SINGLELINE | DT_CENTER);//在兼容DC中间位置输出字符串
			//这样以来我们就相当于把hbmp这个位图加上了文字标注,我们可以把这个增加了文字标注的位图保存起来.一个简单的图像处理基本就OK了.

			BLENDFUNCTION bf = { AC_SRC_OVER, 0, 128, AC_SRC_ALPHA };
			ms_pCurTipWnd->UpdateLayeredWindow(hdc, &CPoint(0, 0), &sz, hMemDC, &CPoint(0, 0), LWA_ALPHA | LWA_COLORKEY, &bf, LWA_ALPHA);

			::SelectObject(hMemDC, hOldBitmap);
			::DeleteObject(hBitmap);
			::DeleteDC(hMemDC);
			ms_pCurTipWnd->ReleaseDC(hdc);
			ms_pCurTipWnd->SetWindowPos(HWND_TOPMOST, 100, 100, 0, 0, SWP_NOSIZE | SWP_NOSENDCHANGING | SWP_NOOWNERZORDER | SWP_SHOWWINDOW | SWP_NOACTIVATE);
		} while (false);
		return bRet;
	}

	void DMTooltipReImpl::OnPaint(HDC hdc)
	{
		do 
		{
			CRect rcClient;
			GetClientRect(&rcClient);

	/*		HBRUSH hBrush = CreateSolidBrush(GetSysColor(COLOR_INFOBK));
			HGDIOBJ hOld = SelectObject(hdc,hBrush);
			Rectangle(hdc,rcClient.left,rcClient.top,rcClient.right,rcClient.bottom);
			SelectObject(hdc,hOld);
			DeleteObject(hBrush); */

			HPEN hPen = CreatePen(PS_SOLID, 1, RGB(190, 190, 190));
			HGDIOBJ hOldPen = SelectObject(hdc, hPen);
		//	Rectangle(hdc, rcClient.left, rcClient.top, rcClient.right, rcClient.bottom);
			::RoundRect(hdc, rcClient.left, rcClient.top, rcClient.right, rcClient.bottom, 3, 3);

			rcClient.DeflateRect(MARGIN_TIP * 3 / 2 + 1, MARGIN_TIP);
			SetBkMode(hdc,TRANSPARENT);
			HGDIOBJ hOldFont = SelectObject(hdc,m_font);
			::DrawText(hdc,m_strTip,-1,&rcClient,DT_WORDBREAK);
			SelectObject(hdc,hOldFont);

			SelectObject(hdc, hOldPen);
			DeleteObject(hPen);
		} while (false);
	}

	void DMTooltipReImpl::OnTimer(UINT_PTR idEvent)
	{
		switch (idEvent)
		{
		case TIMERID_DELAY:
			{
				m_bShowing = false;
				KillTimer(TIMERID_DELAY);       
				ShowTooltip(true);
				SetTimer(TIMERID_SPAN,m_iSpanTime);
			}
			break;

		case TIMERID_SPAN:
			{
				m_bShowing = false;
				ShowTooltip(false);
				KillTimer(TIMERID_SPAN);
			}
			break;
		}
	}

	void DMTooltipReImpl::OnSize(UINT nType, DM::CSize size)
	{
		if (!IsIconic())
		{
			DM::CRect rcWnd;
			::GetWindowRect(m_hWnd, &rcWnd);
			::OffsetRect(&rcWnd, -rcWnd.left, -rcWnd.top);
			rcWnd.InflateRect(0, 0, 1, 1);  //lzlong add 不然窗口的左边界和下边界的一个像素会被遮住  看不到边框
			HRGN hWndRgn = ::CreateRoundRectRgn(0, 0, rcWnd.right, rcWnd.bottom, 3, 3);
			SetWindowRgn(hWndRgn, TRUE);
			::DeleteObject(hWndRgn);
		}
		SetMsgHandled(FALSE);
	}

	DMCode DMTooltipReImpl::Update(PDMToolTipInfo pInfo)
	{
		DMCode iErr = DM_ECODE_FAIL;
		do 
		{
			if (NULL == pInfo)
			{
				break;
			}
			if (pInfo->strTip.IsEmpty())
			{
				break;
			}
			if (!IsWindow())
			{
				if (!Create())
				{
					break;
				}
			}

			/// 更新数据
			m_rcTarget = pInfo->rcTarget;
			m_strTip   = pInfo->strTip;
			m_strTip.Replace(L"\\r\\n",L"\r\n");//什么XML中\R\N不能被直接识别，因为XML文件中的"/n",会被认为是一个字符串"///n"是两个字符'//'和'/n'，而不是转义字符"/n",
			m_strTip.Replace(L"\\n",L"\\r");
			m_iDelayTime = pInfo->iDelayTime;
			m_iSpanTime  = pInfo->iSpanTime;

			if (IsWindowVisible())
			{
				ShowTooltip(true);
			}

			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}

	DMCode DMTooltipReImpl::Hide()
	{
		DMCode iErr = DM_ECODE_FAIL;
		do 
		{
			if (!IsWindow())
			{
				//if (!Create())// 这时不要创建了，因为隐藏也可能发生在宿主窗口销毁后，这时消息循环可能没处理完
				{
					break;
				}
			}
			ShowTooltip(false);
			iErr = DM_ECODE_OK;
		} while (false);
		return iErr;
	}

	DMCode DMTooltipReImpl::Clear()
	{
		if (IsWindow())
		{
			ShowTooltip(false);
			DestroyWindow();
		}
		
		return DM_ECODE_OK;
	}

	DMCode DMTooltipReImpl::RelayEvent(const MSG *pMsg)
	{
		switch (pMsg->message)
		{
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
		case WM_MBUTTONDOWN:
			OnTimer(TIMERID_SPAN);// 直接隐藏
			break;
		case WM_MOUSEMOVE:
			{
				CPoint pt(GET_X_LPARAM(pMsg->lParam),GET_Y_LPARAM(pMsg->lParam));
				if (!m_rcTarget.PtInRect(pt))
				{
					OnTimer(TIMERID_SPAN);// 直接隐藏
				}
				else if (!IsWindowVisible() && !m_strTip.IsEmpty() && !m_bShowing)
				{
					KillTimer(TIMERID_DELAY);
					SetTimer(TIMERID_DELAY,m_iDelayTime);    
					m_bShowing = true;
					::ClientToScreen(pMsg->hwnd,&pt);
					SetWindowPos(0,pt.x,pt.y+24,0,0,SWP_NOSIZE|SWP_NOZORDER|SWP_NOSENDCHANGING|SWP_NOACTIVATE);
				}
			}
			break;
		case WM_MOUSELEAVE:
			{
				CPoint pt(GET_X_LPARAM(pMsg->lParam),GET_Y_LPARAM(pMsg->lParam));
				if (!m_rcTarget.PtInRect(pt))
				{
					OnTimer(TIMERID_SPAN);// 直接隐藏
				}
			}
			break;
		}
		return DM_ECODE_OK;
	}

	BOOL DMTooltipReImpl::PreTranslateMessage(MSG* pMsg)
	{
		if (IsWindow()) 
		{
			RelayEvent(pMsg);
		}
		return FALSE;
	}

	void DMTooltipReImpl::ShowTooltip(bool bShow)
	{
		do 
		{
			if (!bShow)
			{
				m_bShowing = false;
				m_rcTarget.SetRect(0,0,0,0);
				m_strTip = L"";
				if (!IsWindowVisible())
				{
					break;
				}
				ShowWindow(SW_HIDE);
				KillTimer(TIMERID_DELAY);
				KillTimer(TIMERID_SPAN);
				break;
			}

			if (m_strTip.IsEmpty())
			{
				break;
			}
			DMAutoDC hdc;
			CRect rcText(0,0,500,1000);
			HFONT oldFont = (HFONT)SelectObject(hdc,m_font);
			::DrawText(hdc,m_strTip,-1,&rcText,DT_CALCRECT|DT_LEFT|DT_WORDBREAK);// 计算文本大小
			SelectObject(hdc,oldFont);
			CRect rcWnd;
			GetWindowRect(&rcWnd);
			rcWnd.right  = rcWnd.left+rcText.right+3*MARGIN_TIP+1;
			rcWnd.bottom = rcWnd.top+rcText.bottom+2*MARGIN_TIP;
			HMONITOR hMonitor = ::MonitorFromRect(&rcWnd, MONITOR_DEFAULTTONEAREST);
			if (NULL != hMonitor)
			{
				MONITORINFO mi = {sizeof(MONITORINFO)};
				::GetMonitorInfo(hMonitor, &mi);
				CRect rcWork = mi.rcWork;
				if (rcWork.left < rcWork.right)
				{// 简单的支持位移在屏幕内
					if (rcWnd.right > rcWork.right) 
						rcWnd.OffsetRect(rcWork.right - rcWnd.right, 0);
					if (rcWnd.bottom > rcWork.bottom)
						rcWnd.OffsetRect(0, rcWork.bottom - rcWnd.bottom);
					if (rcWnd.left < rcWork.left)
						rcWnd.OffsetRect(rcWork.left - rcWnd.left, 0);
					if (rcWnd.top < rcWork.top)
						rcWnd.OffsetRect(0, rcWork.top - rcWnd.top);
				}
			}
			SetWindowPos(HWND_TOPMOST,rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height(),SWP_NOSENDCHANGING|SWP_SHOWWINDOW|SWP_NOACTIVATE);
		} while (false);
	}

}//namespace DM