#include "StdAfx.h"
#include "DUIAlignmentFrame.h"

DUIAlignmentFrame::DUIAlignmentFrame()
{
	m_Clr = PBGRA(0, 0xff, 0xff, 0xff);
	m_uAlignment = AlignmentLeft;
}

DUIAlignmentFrame::~DUIAlignmentFrame()
{
}

void DUIAlignmentFrame::SetRootWndRect(const CRect& rect)
{
	m_RootWndRect = rect;
}

void DUIAlignmentFrame::SetAlignmentType(AlignmentType type)
{
	m_uAlignment = type;
}

void DUIAlignmentFrame::DM_OnPaint(IDMCanvas* pCanvas)
{
	POINT point[2];
	if (AlignmentLeft == m_uAlignment)
	{
		point[0] = { m_rcWindow.left, m_RootWndRect.top };
		point[1] = { m_rcWindow.left, m_RootWndRect.bottom };
	}
	else if(AlignmentRight == m_uAlignment)
	{
		point[0] = { m_rcWindow.right, m_RootWndRect.top };
		point[1] = { m_rcWindow.right, m_RootWndRect.bottom };
	}
	else if (AlignmentTop == m_uAlignment)
	{
		point[0] = { m_RootWndRect.left, m_rcWindow.top };
		point[1] = { m_RootWndRect.right, m_rcWindow.top };
	}
	else if (AlignmentBottom == m_uAlignment)
	{
		point[0] = { m_RootWndRect.left, m_rcWindow.bottom };
		point[1] = { m_RootWndRect.right, m_rcWindow.bottom };
	}
	AutoDrawDotLine(pCanvas, m_Clr, PS_DOT, 1, point, 2);
}